# emboss-today

Rapid manufacturing of embossing tooling for permanently marking thin plastic.

(c) Massachusetts Institute of Technology 2020

This work may be reproduced, modified, distributed, performed, and displayed for any purpose, but must acknowledge the emboss-today project. Copyright is retained and must be preserved. The work is provided as-is; no warranty is provided, and users accept all liability.

![101st_impression](img/url_multitest.jpg)

![mfg_tool](img/url_tool.jpg)

## status
This project has been shelved as face shield mass manufacturing scaled up faster than expected. The method is still relevant for quick-turn pilot scale runs of thin plastic products.

## need
As part of our covid-19 face shield production efforts, we need to permanently mark units that end up in hospitals with a few basic identifiers: a manufacturing batch number and a shortened URL for assembly instructions. The Zund needs to spend its time cutting rather than marking, and doctors have complained about the smell of laser-engraved text.

## rapid embossing tool manufacturing
Several letters were cut out of 0.5 mm Cu stock using our nanosecond laser micromachining system:

![oxfording](img/oxfording.mp4)

![tool_in_oxford](img/tool_in_oxford.jpg)

The coupons were punched out but the letters were left in place for electrochemical deburring in 14M H3PO4, ~1 minute, 3 VDC:

![tool_deburr](img/tool_deburr.jpg)

Letters were then carefully punched out using a sharp probe under the Lynx Evo inspection microscope. Each 6 mm letter was sanded on two opposing sides to remove remaining cut debris in preparation for soldering. A piece of 0.02 mm Cu sheet was cut with sheet metal snips, fluxed, and the letters carefully soldered on using SAC305 solder:

![tool_soldered](img/tool_soldered.jpg)

The tool was cleaned with IPA and attached to a desktop rack-and-pinion-style press using double-sided tape:

![mfg_tool](img/mfg_tool.jpg)

A piece of silicone rubber was similarly attached opposite the tool to prevent bottoming out on the metal platen:

![mfg_press](img/mfg_press.jpg)

The press was tested 100 times on a scrap of 0.5 mm PETG:

![mfg_100_tests](img/mfg_100_tests.jpg)

The 101st test was performed on a scrap of 0.75 mm polycarbonate:

![101st_impression](img/emboss-today_101st.jpg)

Later tests updated this process as follows:
- 0.25 mm Cu
- 5 mm letter height
- no electrochemical deburr
- sanding bottom rather than sides of letters
- reflowing solder under letters rather than tacking sides

This updated process was used to fabricate a tool with the Project Manus face shield URL, minus dots (as these were lost). Laser micromachining took 40 minutes, while manual assembly took roughly 30 minutes:

![url_tool](img/url_tool.jpg)

The tool was mounted with double-sided tape on a cold rolled mild steel bar, which was held to the press using NdFeB magnets (flipped sideways above for visibility). The tool produced acceptable results in 0.020" PETG:

![url_multitest](img/url_multitest.jpg)

Embossing was also performed on stock with both layers of protective film still in place; prior to film removal, the URL was legible:

![url_protectivefilm1](img/url_protectivefilm1.jpg)

However, legibility was reduced once the film was removed, suggesting film should be selectively removed from shields prior to stamping:

![url_protectivefilm2](img/url_protectivefilm2.jpg)
